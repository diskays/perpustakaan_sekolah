-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2022 at 02:39 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id_buku` varchar(20) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `id_pencipta` varchar(20) NOT NULL,
  `tahun_terbit` int(20) NOT NULL,
  `jumlah_halaman` int(20) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id_buku`, `judul`, `id_pencipta`, `tahun_terbit`, `jumlah_halaman`, `created_at`, `updated_at`) VALUES
('12345678', 'Harry Potter 1', '123ASD40', 2019, 2000, '2022-01-20', '2022-01-20'),
('12345678121', 'Harry Potter Deathly Hallows', '123ASD40', 2000, 2000, '2022-01-20', '2022-01-20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('diskayunita7@gmail.com', '$2y$10$FNB6Yse.JbUVX4bwzjTFC.dCzcGouHb931j4e5dWvWQeLw9QDphJu', '2022-01-20 18:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `pencipta_buku`
--

CREATE TABLE `pencipta_buku` (
  `id_pencipta` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pencipta_buku`
--

INSERT INTO `pencipta_buku` (`id_pencipta`, `nama`, `created_at`, `updated_at`) VALUES
('123ASD40', 'J.K. Rowling', '2022-01-20', '2022-01-20'),
('456JKR12', 'DISKA YUNITA SARI', '2022-01-20', '2022-01-20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `email_verified_at`, `password`, `admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Superadmin', 'superadmin@gmail.com', NULL, '$2y$10$QZCozskNw2LLkgRQ4KOIM.1qcFt/WDuT3iN2zSAZdsx5cVgHpAC12', 'Superadmin', NULL, '2022-01-20 14:50:25', '2022-01-20 14:50:25'),
(2, 'Diska Yunita Sari', 'diskayunita7@gmail.com', NULL, '$2y$10$HjJiRTvGu5u1NM8TiiTPo.7DaN4qTaQBdIPTA6jj2k/rpCTxRLjr.', 'Admin', NULL, '2022-01-20 15:06:30', '2022-01-20 15:07:38'),
(3, 'Diska Yunita Sari', 'user@gmail.com', NULL, '$2y$10$WOLUs7Hs.VqzcYtOBnG.0OnvxE24T76D/Y5g1JWSyaHfeFiyl3cC2', 'User', NULL, '2022-01-20 15:06:50', '2022-01-20 15:06:50'),
(4, 'Diska Yunita Sari', 'diska.yunita@yahoo.com', NULL, '$2y$10$rV7eSpRaKfjyYzP5WcvqLeDuk0xNRgoR9XjD2HCCMS4CFlXi6LX3u', 'Guest', 'mROqwhpDxkjBqRA0o16RwSQ6UM8eZV6aBx1HsGa8ZAjGSst19tL6MO5nI1AP', '2022-01-20 15:15:11', '2022-01-20 17:23:27'),
(5, 'Diska Yunita Sari', 'lalalalala@gmail.com', NULL, '$2y$10$vodRwONQqqcJaO1yPFlT..7ZlmjE8u4W0DP9K73ZUEmxXUbuPqCei', 'Guest', NULL, '2022-01-20 18:18:27', '2022-01-20 18:18:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pencipta_buku`
--
ALTER TABLE `pencipta_buku`
  ADD PRIMARY KEY (`id_pencipta`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
